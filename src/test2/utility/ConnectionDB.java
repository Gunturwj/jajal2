/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2.utility;

/**
 *
 * @author Candra Mega
 */
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionDB {
    private static Connection conn;
    
    public static Connection getConnection(){
        if (conn == null) {
            try {
                 Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jajal", "root", "");
//                conn = DriverManager.getConnection(null, null, null)
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return conn;
    }
}
