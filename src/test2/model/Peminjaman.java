/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Candra Mega
 */
@Entity
@Table(name = "peminjaman")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Peminjaman.findAll", query = "SELECT p FROM Peminjaman p"),
    @NamedQuery(name = "Peminjaman.findByKodepinjam", query = "SELECT p FROM Peminjaman p WHERE p.kodepinjam = :kodepinjam"),
    @NamedQuery(name = "Peminjaman.findByTanggalpinjam", query = "SELECT p FROM Peminjaman p WHERE p.tanggalpinjam = :tanggalpinjam"),
    @NamedQuery(name = "Peminjaman.findByTanggalkembali", query = "SELECT p FROM Peminjaman p WHERE p.tanggalkembali = :tanggalkembali")})
public class Peminjaman implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "kodepinjam")
    private String kodepinjam;
    @Column(name = "tanggalpinjam")
    @Temporal(TemporalType.DATE)
    private Date tanggalpinjam;
    @Column(name = "tanggalkembali")
    @Temporal(TemporalType.DATE)
    private Date tanggalkembali;
    @JoinColumn(name = "Kode_Buku", referencedColumnName = "kode")
    @OneToOne
    private Buku kodeBuku;
    @JoinColumn(name = "Kode_Anggota", referencedColumnName = "Kode_Anggota")
    @OneToOne
    private Anggota kodeAnggota;

    public Peminjaman() {
    }

    public Peminjaman(String kodepinjam) {
        this.kodepinjam = kodepinjam;
    }

    public String getKodepinjam() {
        return kodepinjam;
    }

    public void setKodepinjam(String kodepinjam) {
        this.kodepinjam = kodepinjam;
    }

    public Date getTanggalpinjam() {
        return tanggalpinjam;
    }

    public void setTanggalpinjam(Date tanggalpinjam) {
        this.tanggalpinjam = tanggalpinjam;
    }

    public Date getTanggalkembali() {
        return tanggalkembali;
    }

    public void setTanggalkembali(Date tanggalkembali) {
        this.tanggalkembali = tanggalkembali;
    }

    public Buku getKodeBuku() {
        return kodeBuku;
    }

    public void setKodeBuku(Buku kodeBuku) {
        this.kodeBuku = kodeBuku;
    }

    public Anggota getKodeAnggota() {
        return kodeAnggota;
    }

    public void setKodeAnggota(Anggota kodeAnggota) {
        this.kodeAnggota = kodeAnggota;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kodepinjam != null ? kodepinjam.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Peminjaman)) {
            return false;
        }
        Peminjaman other = (Peminjaman) object;
        if ((this.kodepinjam == null && other.kodepinjam != null) || (this.kodepinjam != null && !this.kodepinjam.equals(other.kodepinjam))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test2.model.Peminjaman[ kodepinjam=" + kodepinjam + " ]";
    }
    
}
