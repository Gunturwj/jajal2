/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Candra Mega
 */
@Entity
@Table(name = "pengembalian")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pengembalian.findAll", query = "SELECT p FROM Pengembalian p"),
    @NamedQuery(name = "Pengembalian.findByKodeKembali", query = "SELECT p FROM Pengembalian p WHERE p.kodeKembali = :kodeKembali"),
    @NamedQuery(name = "Pengembalian.findByTanggalKembali", query = "SELECT p FROM Pengembalian p WHERE p.tanggalKembali = :tanggalKembali"),
    @NamedQuery(name = "Pengembalian.findByJatuhTempo", query = "SELECT p FROM Pengembalian p WHERE p.jatuhTempo = :jatuhTempo"),
    @NamedQuery(name = "Pengembalian.findByDendaperhari", query = "SELECT p FROM Pengembalian p WHERE p.dendaperhari = :dendaperhari"),
    @NamedQuery(name = "Pengembalian.findByJumlahHar", query = "SELECT p FROM Pengembalian p WHERE p.jumlahHar = :jumlahHar"),
    @NamedQuery(name = "Pengembalian.findByJumlahDenda", query = "SELECT p FROM Pengembalian p WHERE p.jumlahDenda = :jumlahDenda")})
public class Pengembalian implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Kode_Kembali")
    private String kodeKembali;
    @Column(name = "Tanggal_Kembali")
    @Temporal(TemporalType.DATE)
    private Date tanggalKembali;
    @Column(name = "Jatuh_Tempo")
    @Temporal(TemporalType.DATE)
    private Date jatuhTempo;
    @Column(name = "Denda_per_hari")
    private Integer dendaperhari;
    @Column(name = "Jumlah_Har")
    private Integer jumlahHar;
    @Column(name = "Jumlah_Denda")
    private Integer jumlahDenda;
    @JoinColumn(name = "Kode_Anggota", referencedColumnName = "Kode_Anggota")
    @OneToOne
    private Anggota kodeAnggota;
    @JoinColumn(name = "Kode_Buku", referencedColumnName = "kode")
    @OneToOne
    private Buku kodeBuku;

    public Pengembalian() {
    }

    public Pengembalian(String kodeKembali) {
        this.kodeKembali = kodeKembali;
    }

    public String getKodeKembali() {
        return kodeKembali;
    }

    public void setKodeKembali(String kodeKembali) {
        this.kodeKembali = kodeKembali;
    }

    public Date getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(Date tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public Date getJatuhTempo() {
        return jatuhTempo;
    }

    public void setJatuhTempo(Date jatuhTempo) {
        this.jatuhTempo = jatuhTempo;
    }

    public Integer getDendaperhari() {
        return dendaperhari;
    }

    public void setDendaperhari(Integer dendaperhari) {
        this.dendaperhari = dendaperhari;
    }

    public Integer getJumlahHar() {
        return jumlahHar;
    }

    public void setJumlahHar(Integer jumlahHar) {
        this.jumlahHar = jumlahHar;
    }

    public Integer getJumlahDenda() {
        return jumlahDenda;
    }

    public void setJumlahDenda(Integer jumlahDenda) {
        this.jumlahDenda = jumlahDenda;
    }

    public Anggota getKodeAnggota() {
        return kodeAnggota;
    }

    public void setKodeAnggota(Anggota kodeAnggota) {
        this.kodeAnggota = kodeAnggota;
    }

    public Buku getKodeBuku() {
        return kodeBuku;
    }

    public void setKodeBuku(Buku kodeBuku) {
        this.kodeBuku = kodeBuku;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kodeKembali != null ? kodeKembali.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pengembalian)) {
            return false;
        }
        Pengembalian other = (Pengembalian) object;
        if ((this.kodeKembali == null && other.kodeKembali != null) || (this.kodeKembali != null && !this.kodeKembali.equals(other.kodeKembali))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test2.model.Pengembalian[ kodeKembali=" + kodeKembali + " ]";
    }
    
}
