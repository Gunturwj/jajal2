/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2.service;

import java.sql.SQLException;
import java.util.List;
import test2.dao.BukuDao;
import test2.model.Buku;

/**
 *
 * @author Candra Mega
 */
public class BukuService {
    
    BukuDao dao = new BukuDao();

    public void insert(Buku buku) throws SQLException {
        dao.insert(buku);
    }

    public void update(Buku buku) throws SQLException {
        dao.update(buku);
    }

    public void delete(String pid) throws SQLException {
        dao.delete(pid);
    }

    public List selectAll() throws SQLException {
        return dao.selectAll();
    }

    public Buku getByKode(String kode) throws SQLException {
        return dao.getByKode(kode);
    }
    
}
