/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Candra Mega
 */
@Entity
@Table(name = "buku")
@XmlRootElement

//@NamedQueries({
  //  @NamedQuery(name = "Buku.findAll", query = "SELECT b FROM Buku b"),
  //  @NamedQuery(name = "Buku.findByKode", query = "SELECT b FROM Buku b WHERE b.kode = :kode"),
  //  @NamedQuery(name = "Buku.findByJudul", query = "SELECT b FROM Buku b WHERE b.judul = :judul"),
  //  @NamedQuery(name = "Buku.findByPenerbit", query = "SELECT b FROM Buku b WHERE b.penerbit = :penerbit"),
  //  @NamedQuery(name = "Buku.findByPengarang", query = "SELECT b FROM Buku b WHERE b.pengarang = :pengarang"),
  //  @NamedQuery(name = "Buku.findByTahun", query = "SELECT b FROM Buku b WHERE b.tahun = :tahun")})
public class Buku implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    @OneToOne(mappedBy = "kodeBuku")
    private Pengembalian pengembalian;
    @OneToOne(mappedBy = "kodeBuku")
    private Peminjaman peminjaman;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "kode")
    private String kode;
    @Basic(optional = false)
    @Column(name = "judul")
    private String judul;
    @Basic(optional = false)
    @Column(name = "penerbit")
    private String penerbit;
    @Basic(optional = false)
    @Column(name = "pengarang")
    private String pengarang;
    @Basic(optional = false)
    @Column(name = "tahun")
    private String tahun;

    public Buku() {
    }

    public Buku(String kode) {
        this.kode = kode;
    }

    public Buku(String kode, String judul, String penerbit, String pengarang, String tahun) {
        this.kode = kode;
        this.judul = judul;
        this.penerbit = penerbit;
        this.pengarang = pengarang;
        this.tahun = tahun;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        String oldKode = this.kode;
        this.kode = kode;
        changeSupport.firePropertyChange("kode", oldKode, kode);
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        String oldJudul = this.judul;
        this.judul = judul;
        changeSupport.firePropertyChange("judul", oldJudul, judul);
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        String oldPenerbit = this.penerbit;
        this.penerbit = penerbit;
        changeSupport.firePropertyChange("penerbit", oldPenerbit, penerbit);
    }

    public String getPengarang() {
        return pengarang;
    }

    public void setPengarang(String pengarang) {
        String oldPengarang = this.pengarang;
        this.pengarang = pengarang;
        changeSupport.firePropertyChange("pengarang", oldPengarang, pengarang);
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        String oldTahun = this.tahun;
        this.tahun = tahun;
        changeSupport.firePropertyChange("tahun", oldTahun, tahun);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kode != null ? kode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Buku)) {
            return false;
        }
        Buku other = (Buku) object;
        if ((this.kode == null && other.kode != null) || (this.kode != null && !this.kode.equals(other.kode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "test2.model.Buku[ kode=" + kode + " ]";
    }

    public Pengembalian getPengembalian() {
        return pengembalian;
    }

    public void setPengembalian(Pengembalian pengembalian) {
        this.pengembalian = pengembalian;
    }

    public Peminjaman getPeminjaman() {
        return peminjaman;
    }

    public void setPeminjaman(Peminjaman peminjaman) {
        this.peminjaman = peminjaman;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
