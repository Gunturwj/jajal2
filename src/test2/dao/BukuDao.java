/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test2.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import test2.model.Buku;
import test2.utility.ConnectionDB;

/**
 *
 * @author Candra Mega
 */
public class BukuDao {
    
    public void insert(Buku buku) throws SQLException{
        String kode = buku.getKode();
        String judul = buku.getJudul();
        String pengarang = buku.getPengarang();
        String penerbit = buku.getPenerbit();
        String tahun = buku.getTahun();
        Connection conn = ConnectionDB.getConnection();
        Statement stat = conn.createStatement();
        stat.execute("INSERT INTO buku (kode, judul, pengarang, penerbit, tahun) VALUES ('"+kode+"', '"+judul+"', '"+pengarang+"', '"+penerbit+"', '"+tahun+"')");
        stat.close();
    }
    
    public void update(Buku buku) throws SQLException {
        String kode = buku.getKode();
        String judul = buku.getJudul();
        String pengarang = buku.getPengarang();
        String penerbit = buku.getPenerbit();
        String tahun = buku.getTahun();
        Connection conn = ConnectionDB.getConnection();
        Statement stat = conn.createStatement();
        stat.execute("UPDATE buku SET kode='" + kode +"',judul='" + judul + "',pengarang='" + pengarang + "',penerbit='" + penerbit + "',tahun='" + tahun+"'");
        stat.close();
    }
    
    public void delete(String pid) throws SQLException {
        Connection conn = ConnectionDB.getConnection();
        Statement st = conn.createStatement();
        st.execute("DELETE FROM buku WHERE kode=" + pid);
        st.close();
    }
    
    public List selectAll() throws SQLException {
        List listBuku = new ArrayList();
        Connection conn = ConnectionDB.getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select * from buku ");
        while (rs.next()) {
            Buku buku = new Buku();
            String kode = rs.getString("kode");
            String judul = rs.getString("judul");
            String pengarang = rs.getString("pengarang");
            String penerbit = rs.getString("penerbit");
            String tahun = rs.getString("tahun");
            buku.setKode(kode);
            buku.setJudul(judul);
            buku.setPengarang(pengarang);
            buku.setPenerbit(penerbit);
            buku.setTahun(tahun);
            listBuku.add(buku);
        }
        rs.close();
        return listBuku;
    }
    
    public Buku getByKode(String kode) throws SQLException {
        Buku buku = new Buku();
        Connection conn = ConnectionDB.getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select * from buku where kode = " + kode);
        while (rs.next()) {
            String judul = rs.getString("judul");
            String pengarang = rs.getString("pengarang");
            String penerbit = rs.getString("penerbit");
            String tahun = rs.getString("tahun");
            buku.setKode(kode);
            buku.setJudul(judul);
            buku.setPengarang(pengarang);
            buku.setPenerbit(penerbit);
            buku.setTahun(tahun);
        }
        rs.close();
        return buku;
    }
}
